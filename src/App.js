import "./App.css";
import React from "react";

function App() {
	const [state, setState] = React.useState({});
	const url = process.env.REACT_APP_ALB_ADDRESS;
	console.log(url);
	
	React.useEffect(() => {
		const callAPI = async () => {
			try {
				const res = await fetch(url);
				const data = await res.json();
				console.log(data);

				setState(data);
			} catch (err) {
				console.log(err);
			}
		};

		callAPI();
	}, []);

	return (
		<div>
			<h1>Message from server:{JSON.stringify(state?.message)}</h1>
		</div>
	);
}

export default App;
